Feature: Logging in
  As a user
  I want to login with my credentials
  So that I can take a full advantage of the site's capabilities

  Background: User is on the log in page
    Given User is on the automationpractice.com website
    When I navigate to the log in page by clicking on the "Sign In" button
    Then I should be on the login page

  Scenario: Verify a web connection security
    Then Page should be served via a secure protocol

  Scenario Outline: Verify that user is able to login with a valid username and password
    Given Valid credentials are known: <login> <password>
    When I fill the form with a valid login and password
      And I submit the form by clicking on the "Sign In" button
    Then I should be logged in
    Examples:
      | login         | password   |
      | test@test.xyz | Password1! |

  Scenario Outline: Verify that user is not able to login with a valid username and invalid password
    Given Valid credentials are known: <login> <password>
    When I fill the form with a valid login and invalid password
      And I submit the form by clicking on the "Sign In" button
    Then I should not be logged in
      And I should receive an information about invalid login or password
      And I should be on the login page
    Examples:
      | login         | password              |
      | test@test.xyz | NotTheRightPassword1! |

  Scenario Outline: Verify that user is not able to login with an invalid username and password
    Given Invalid credentials are known: <login> <password>
    When I fill the form with an invalid login and password
      And I submit the form by clicking on the "Sign In" button
    Then I should not be logged in
      And I should receive an information about invalid login or password
      And I should be on the login page
    Examples:
      | login                    | password              |
      | notexistinguser@test.xyz | NotTheRightPassword1! |

  Scenario: Verify that user is not able to login with a blank username and password
    When I leave username and password fields blank
      And I submit the form by clicking on the "Sign In" button
    Then I should not be logged in
      And I should receive an information about required credentials' missing
      And I should be on the login page

  Scenario Outline: Verify that user is not able to login with a valid username and blank password
    Given Valid login of registered user is known: <login>
    When I fill the form with a valid login and leave the password field blank
      And I submit the form by clicking on the "Sign In" button
    Then I should not be logged in
      And I should receive an information about the password is required
      And I should be on the login page
    Examples:
      | login         |
      | test@test.xyz |

  Scenario Outline: Verify that user is not able to login with an invalid username and blank password
    Given Invalid login is known: <login>
    When I fill the form with an invalid login and leave the password field blank
      And I submit the form by clicking on the "Sign In" button
    Then I should not be logged in
      And I should receive an information about the password is required
      And I should be on the login page
    Examples:
      | login                    |
      | notexistinguser@test.xyz |

  Scenario Outline: Verify results of clicking the BACK browser button after successfully logged in
    Given Valid credentials are known: <login> <password>
    When I fill the form with a valid login and password
      And I submit the form by clicking on the "Sign In" button
      And I click the BACK browser button
    Then I should be logged in
    Examples:
      | login         | password   |
      | test@test.xyz | Password1! |

  Scenario Outline: Verify results of clicking the BACK browser button after successfully logged out
    Given Valid credentials are known: <login> <password>
    When I fill the form with a valid login and password
      And I submit the form by clicking on the "Sign In" button
      And I log out by clicking on the "Sign out" button
      And I click the BACK browser button
    Then I should not be logged in
    Examples:
      | login         | password   |
      | test@test.xyz | Password1! |

  Scenario: Verify possibility of resetting or reminder password
    Then I should be able to reset or remind my password

  Scenario: Verify possibility of submitting the form by pressing the ENTER key
    When I set the focus on any field of the form and press the ENTER key
    Then I should not be logged in
      And I should receive an information about required credentials' missing

  Scenario: Verify if the password is displaying in a masked form
    When I fill the password field with a random value
    Then I should not be able to see the password in a original form

  Scenario: Verify results of trying to demask password by copy-paste the password field's content
    Given I should not be able to see the password in a original form
    When I fill the password field with a random value
      And I select the password field content and try to copy them into clipboard
    Then I should not be able to demask the password by preview the clipboard's content

  Scenario: Verify the fields order in the Sign In form
    Then I should see the form in which login field is in the front of the password