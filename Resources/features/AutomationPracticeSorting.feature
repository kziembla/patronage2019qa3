Feature: Sorting products
  As a user
  I want to sort category's products
  So that I can quickly get what I'm interested of

  Background: User is on the category's products listing page
    Given User is on the automationpractice.com website
    When I navigate to the category with at least two products
    Then Product listing page should be rendered

  Scenario: Verify that user is able to sort products by price (from the cheapest to the most expensive)
    Given Chosen category contains at least two products differing in "price"
    When I choose the "Price: Lowest first" option from the drop-down list
    Then Products should be listed in the right order: "from the cheapest to the most expensive"

  Scenario: Verify that user is able to sort products by price (from the most expensive to the cheapest)
    Given Chosen category contains at least two products differing in "price"
    When I choose the "Price: Highest first" option from the drop-down list
    Then Products should be listed in the right order: "from the most expensive to the cheapest"

  Scenario: Verify that user is able to sort products by name (from A to Z)
    Given Chosen category contains at least two products differing in "name"
    When I choose the "Product Name: A to Z" option from the drop-down list
    Then Products should be listed in the right order: "alphabetical order"

  Scenario: Verify that user is able to sort products by name (from Z to A)
    Given Chosen category contains at least two products differing in "name"
    When I choose the "Product Name: Z to A" option from the drop-down list
    Then Products should be listed in the right order: "reverse to the alphabetical order"

  Scenario: Verify that user is able to sort products by availability (available products first)
    Given Chosen category contains at least two products differing in "availability"
    When I choose the "In stock" option from the drop-down list
    Then Products should be listed in the right order: "available products first"

  Scenario: Verify that user is able to sort products by reference number (from the lowest to the highest)
    Given Chosen category contains at least two products differing in "reference"
    When I choose the "Reference: Lowest first" option from the drop-down list
    Then Products should be listed in the right order: "from the lowest to the highest reference number"

  Scenario: Verify that user is able to sort products by reference number (from the highest to the lowest)
    Given Chosen category contains at least two products differing in "reference"
    When I choose the "Reference: Highest first" option from the drop-down list
    Then Products should be listed in the right order: "from the highest to the lowest reference number"

  Scenario: Verify that the products are properly rendered after switching to default sorting option
    When I choose the "Price: Lowest first" option from the drop-down list
      And I choose the "--" option from the drop-down list
    Then Products should be properly rendered