package com.Intive.Patronage.tests.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.MatcherAssert.assertThat;

public class AutomationPracticePage {

    private WebDriver driver;

    public AutomationPracticePage(final WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // ------------------------------------------------------------------------------------
    // COMMON / MAIN PAGE
    // ------------------------------------------------------------------------------------

    private static String AUTOMATIONPRACTICE_URL = "http://automationpractice.com/index.php";
    private static String siteName = "My Store";

    private static String login = "";
    private static String password = "";
    private static String copiedPassword = "";

    private static String logInPageTitle = "Login";

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Sign in")
    private WebElement signInButton;

    public void openAutomationPracticeWebsite() {
        driver.get(AUTOMATIONPRACTICE_URL);
    }

    public void goToTheSignInPage() {
        signInButton.click();
    }

    // ------------------------------------------------------------------------------------
    // SIGN IN FEATURE
    // ------------------------------------------------------------------------------------

    @FindBy(how = How.LINK_TEXT, using = "Forgot your password?")
    private WebElement passwordRecovery;

    @FindBy(how = How.LINK_TEXT, using = "Sign out")
    private WebElement signOutButton;

    @FindBy(how = How.NAME, using = "email")
    private WebElement emailField;

    @FindBy(how = How.NAME, using = "passwd")
    private WebElement passwordField;

    @FindBy(how = How.NAME, using = "SubmitLogin")
    private WebElement submitLoginButton;

    @FindBy(how = How.CLASS_NAME, using = "alert alert-danger")
    private WebElement alertMessage;

    public void verifyIfPageIsServedViaASecureProtocol() throws InterruptedException {
        String actualURL = driver.getCurrentUrl();
        assertThat("Page should be served via a secure protocol", actualURL.startsWith("https://"));
    }

    public void verifyIfSignInPageIsRendered() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.titleContains(logInPageTitle));
    }

    public void submitTheForm() {
        submitLoginButton.click();
    }

    public void fillTheForm(String login, String password) {
        emailField.sendKeys(login);
        passwordField.sendKeys(password);
    }

    public void verifyIfUserIsLoggedIn() throws InterruptedException {
        assertThat("User should be logged in", driver.getPageSource().contains("Sign out"));
    }

    public void verifyIfUserIsLoggedOut() throws InterruptedException {
        assertThat("User should be logged out", driver.getPageSource().contains("Sign in"));
    }

    public void verifyIfAuthenticationFailed() throws InterruptedException {
        assertThat("User should get a message about authentication failed",
                driver.getPageSource().contains("Authentication failed"));
    }

    public void verifyIfRequiredCredentialsAreMissing() throws InterruptedException {
        assertThat("User should get a message about missing e-mail address",
                driver.getPageSource().contains("An email address required"));
    }

    public void verifyIfPasswordIsMissing() throws InterruptedException {
        assertThat("User should get a message about password which is required",
                driver.getPageSource().contains("Password is required"));
    }

    public void clickTheBackBrowserButton() {
        driver.navigate().back();
    }

    public void signOut() {
        signOutButton.click();
    }

    public void verifyIfPasswordRecoveryOptionIsAvailable() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOf(passwordRecovery));
    }

    public void setTheFocusAndPressEnterKey() {
        emailField.sendKeys(Keys.ENTER);
    }

    public void verifyIfPasswordIsShowInTheMaskedForm() throws InterruptedException {
        assertThat("Password should be displayed in a masked form",
                passwordField.getAttribute("type").equals("password"));
    }

    public void copyPasswordFieldValueToTheClipboard() {
        copiedPassword = passwordField.getText();
    }

    public void compareClipboardContentWithPasswordFieldValue() {
        assertThat("Copying password value should not be possible", copiedPassword.equals(passwordField.getText()));
    }

    public void checkIfLoginFieldIsInTheFrontOfThePassword() {
        assertThat("E-mail field should be in the front of the password",
                emailField.getLocation().y < passwordField.getLocation().y);
    }

    public void fillThePasswordFiledWithARandomValue() {
        passwordField.sendKeys(UUID.randomUUID().toString());
    }

    // ------------------------------------------------------------------------------------
    // SORTING FEATURE
    // ------------------------------------------------------------------------------------

    @FindBy(how = How.ID, using = "selectProductSort")
    private WebElement sortingDropDownList;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Women")
    private WebElement categoryLink;

    public void navigateToTheValidCategory() {
        categoryLink.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='product_list grid row']")));
    }

    public void verifyIfProductListingPageIsRendered() {
        sortingDropDownList.isDisplayed();
    }

    public void verifyIfChosenCategoryContainsAtLeastTwoProductsDifferingIn(String attribute) {

        List<WebElement> productList = null;
        ArrayList<String> attributeList = new ArrayList<>();

        switch (attribute) {
        case "price":
            productList = driver.findElements(By.xpath("//*[starts-with(@class, 'price product-price')][text()]"));
            break;
        case "name":
            productList = driver.findElements(By.xpath("//*[starts-with(@class, 'product-name')][text()]"));
            break;
        case "availability":
            productList = driver.findElements(By.xpath("//*[@itemprop='availability']"));
            break;
        case "reference":
            productList = driver.findElements(By.xpath(
                    "//ul[@class='product_list grid row']//a[@class='product-name'][contains(@href, '?id_product=')]"));
            break;
        }

        for (WebElement element : productList) {
            if (!element.getText().isEmpty()) {
                attributeList.add(element.getText());
            }
        }

        Boolean ifAttributesAreDifferent = new ArrayList<>(new HashSet<>(attributeList)).size() > 1;

        assertThat("Chosen category should contains at least two products that differs in tested attribute",
                ifAttributesAreDifferent);
    }

    public void chooseTheOptionFromTheDropDownList(String chosedOption) {
        Select dropDownList = new Select(sortingDropDownList);
        dropDownList.selectByVisibleText(chosedOption);

        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

        ((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, sortingDropDownList);

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//*[@class='product_list grid row'][contains(@style, 'opacity: 1')]")));
    }

    public void verifyIfProductsAreListedInTheRightOrder(String order) {

        ArrayList<String> obtainedList = new ArrayList<>();
        String regex = null;
        Boolean listShouldBeProcessed = false;
        String sortingOrder = "";

        switch (order) {
        case "from the cheapest to the most expensive":
            sortingOrder = "asc";
        case "from the most expensive to the cheapest":
            regex = "(?<=<div class=\"content_price\" itemprop=\"offers\" itemscope=\"\" itemtype=\"http:\\/\\/schema\\.org\\/Offer\">\\n<span itemprop=\"price\" class=\"price product-price\">\\n)(.*)(?=<\\/span>)";
            if (sortingOrder.isEmpty())
                sortingOrder = "desc";
            break;

        case "alphabetical order":
            sortingOrder = "asc";
        case "reverse to the alphabetical order":
            regex = "(?<=title=\")(.*)(?=\" itemprop=\"url\">)";
            listShouldBeProcessed = true;
            if (sortingOrder.isEmpty())
                sortingOrder = "desc";
            break;

        case "available products first":
            regex = "(?<=<link itemprop=\"availability\" href=\"http:\\/\\/schema.org\\/)(.*)(?=\">)";
            sortingOrder = "asc";
            // According to Schema: In Stock -> Out of Order
            break;

        case "from the lowest to the highest reference number":
            sortingOrder = "asc";
        case "from the highest to the lowest reference number":
            regex = "(?<=<h5 itemprop=\"name\">\\n<a class=\"product-name\" href=\"http:\\/\\/automationpractice.com\\/index.php\\?id_product=)(.*)(?=&amp;)";
            if (sortingOrder.isEmpty())
                sortingOrder = "desc";
            break;
        }

        String html = ((JavascriptExecutor) driver).executeScript("return document.body.innerHTML;").toString().trim()
                .replaceAll(" +", " ").replaceAll("\t", "").replaceAll("\r", "");

        final Matcher m = Pattern.compile(regex).matcher(html);

        while (m.find()) {
            obtainedList.add(m.group(1));
        }

        if (listShouldBeProcessed) {
            for (int i = 0; i < obtainedList.size(); i++) {
                if (i % 2 == 0) {
                    obtainedList.remove(i);
                }
            }
        }

        List<String> sortedList = new ArrayList<String>(obtainedList);

        switch (sortingOrder) {
        case "asc":
            Collections.sort(sortedList);
            break;
        case "desc":
            Collections.reverse(sortedList);
        }

        assertThat("Products are not listed in a correct order", sortedList.equals(obtainedList));
    }

    public void verifyIfProductsAreProperlyRendered() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='product_list grid row']")));
    }
}
