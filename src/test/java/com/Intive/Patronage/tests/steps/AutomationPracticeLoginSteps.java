package com.Intive.Patronage.tests.steps;

import com.Intive.Patronage.tests.DriverFactory;
import com.Intive.Patronage.tests.pages.AutomationPracticePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AutomationPracticeLoginSteps extends DriverFactory {

    AutomationPracticePage automationPractice = new AutomationPracticePage(driver);

    private String login;
    private String password;

    // ------------------------------------------------------------------------------------
    // GIVEN SECTION
    // ------------------------------------------------------------------------------------

    @Given("^User is on the automationpractice.com website$")
    public void userIsOnTheAutomationPracticeComWebsite() {
        automationPractice.openAutomationPracticeWebsite();
    }

    @Given("^Valid credentials are known: (.*) (.*)$")
    public void validCredentialsAreKnown(String validLogin, String validPassword) {
        login = validLogin;
        password = validPassword;
    }

    @Given("^Invalid credentials are known: (.*) (.*)$")
    public void invalidCredentialsAreKnown(String invalidLogin, String invalidPassword) {
        login = invalidLogin;
        password = invalidPassword;
    }

    @Given("^Valid login of registered user is known: (.*)$")
    public void validLoginOfRegisteredUserIsKnown(String validLogin) {
        login = validLogin;
    }

    @Given("^Invalid login is known: (.*)$")
    public void invalidLoginIsKnown(String validLogin) {
        login = validLogin;
    }

    // ------------------------------------------------------------------------------------
    // WHEN SECTION
    // ------------------------------------------------------------------------------------

    @When("^I navigate to the log in page by clicking on the \"Sign In\" button$")
    public void iNavigateToTheLogInPageByClickingOnTheSignInButton() {
        automationPractice.goToTheSignInPage();
    }

    @When("^I fill the form with a valid login and password$")
    public void iFillTheFormWithAValidLoginAndPassword() {
        automationPractice.fillTheForm(login, password);
    }

    @When("^I fill the form with a valid login and invalid password$")
    public void iFillTheFormWithAValidLoginAndInvalidPassword() {
        automationPractice.fillTheForm(login, password);
    }

    @When("^I fill the form with an invalid login and password$")
    public void iFillTheFormWithAnInvalidLoginAndPassword() {
        automationPractice.fillTheForm(login, password);
    }

    @When("^I leave username and password fields blank$")
    public void iLeaveUsernameAndPasswordFieldsBlank() {
        automationPractice.fillTheForm("", "");
    }

    @When("^I fill the form with a valid login and leave the password field blank$")
    public void iFillTheFormWithAValidLoginAndLeaveThePasswordFieldBlank() {
        automationPractice.fillTheForm(login, "");
    }

    @When("^I fill the form with an invalid login and leave the password field blank$")
    public void iFillTheFormWithAnInvalidLoginAndLeaveThePasswordFieldBlank() {
        automationPractice.fillTheForm(login, "");
    }

    @When("^I fill the password field with a random value$")
    public void iFillThePasswordFieldWithARandomValue() {
        automationPractice.fillThePasswordFiledWithARandomValue();
    }

    @When("^I set the focus on any field of the form and press the ENTER key$")
    public void iSetTheFocusOnAnyFieldOfTheFormAndPressTheENTERKey() {
        automationPractice.setTheFocusAndPressEnterKey();
    }

    @And("^I submit the form by clicking on the \"Sign In\" button$")
    public void iSubmitTheFormByClickingOnTheSignInButton() {
        automationPractice.submitTheForm();
    }

    @And("^I click the BACK browser button$")
    public void iClickTheBACKBrowserButton() {
        automationPractice.clickTheBackBrowserButton();
    }

    @And("^I log out by clicking on the \"Sign out\" button$")
    public void iLogOutByClickingOnTheButton() {
        automationPractice.signOut();
    }

    // ------------------------------------------------------------------------------------
    // THEN SECTION
    // ------------------------------------------------------------------------------------

    @Then("^I should be on the login page$")
    public void iShouldBeOnTheLoginPage() throws Throwable {
        automationPractice.verifyIfSignInPageIsRendered();
    }

    @Then("^Page should be served via a secure protocol$")
    public void pageShouldBeServedViaASecureProtocol() throws Throwable {
        automationPractice.verifyIfPageIsServedViaASecureProtocol();
    }

    @Then("^I should be logged in$")
    public void iShouldBeLoggedIn() throws Throwable {
        automationPractice.verifyIfUserIsLoggedIn();
    }

    @Then("^I should not be logged in$")
    public void iShouldNotBeLoggedIn() throws Throwable {
        automationPractice.verifyIfUserIsLoggedOut();
    }

    @Then("^I should be able to reset or remind my password$")
    public void iShouldBeAbleToResetOrRemindMyPassword() throws Throwable {
        automationPractice.verifyIfPasswordRecoveryOptionIsAvailable();
    }

    @Then("^I should not be able to see the password in a original form$")
    public void iShouldNotBeAbleToSeeThePasswordInAOriginalForm() throws Throwable {
        automationPractice.verifyIfPasswordIsShowInTheMaskedForm();
    }

    @Then("^I should not be able to demask the password by preview the clipboard's content$")
    public void iShouldNotBeAbleToDemaskThePasswordByPreviewTheClipboardSContent() throws Throwable {
        automationPractice.compareClipboardContentWithPasswordFieldValue();
    }

    @Then("^I should see the form in which login field is in the front of the password$")
    public void iShouldSeeTheFormInWhichLoginFieldIsInTheFrontOfThePassword() throws Throwable {
        automationPractice.checkIfLoginFieldIsInTheFrontOfThePassword();
    }

    @And("^I should receive an information about invalid login or password$")
    public void iShouldReceiveAnInformationAboutInvalidLoginOrPassword() throws Throwable {
        automationPractice.verifyIfAuthenticationFailed();
    }

    @And("^I should receive an information about required credentials' missing$")
    public void iShouldReceiveAnInformationAboutRequiredCredentialsMissing() throws Throwable {
        automationPractice.verifyIfRequiredCredentialsAreMissing();
    }

    @And("^I should receive an information about the password is required$")
    public void iShouldReceiveAnInformationAboutThePasswordIsRequired() throws Throwable {
        automationPractice.verifyIfPasswordIsMissing();
    }

    @And("^I select the password field content and try to copy them into clipboard$")
    public void iSelectThePasswordFieldContentAndTryToCopyThemIntoClipboard() throws Throwable {
        automationPractice.copyPasswordFieldValueToTheClipboard();
    }
}