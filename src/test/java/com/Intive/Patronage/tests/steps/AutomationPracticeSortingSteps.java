package com.Intive.Patronage.tests.steps;

import com.Intive.Patronage.tests.DriverFactory;
import com.Intive.Patronage.tests.pages.AutomationPracticePage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AutomationPracticeSortingSteps extends DriverFactory {

    AutomationPracticePage automationPractice = new AutomationPracticePage(driver);

    // ------------------------------------------------------------------------------------
    // GIVEN SECTION
    // ------------------------------------------------------------------------------------

    @Given("^Chosen category contains at least two products differing in \"([^\"]*)\"$")
    public void chosenCategoryContainsAtLeastTwoProductsDifferingIn(String attribute) {
        automationPractice.verifyIfChosenCategoryContainsAtLeastTwoProductsDifferingIn(attribute);
    }

    // ------------------------------------------------------------------------------------
    // WHEN SECTION
    // ------------------------------------------------------------------------------------

    @When("^I navigate to the category with at least two products$")
    public void iNavigateToTheCategoryWithAtLeastTwoProducts() {
        automationPractice.navigateToTheValidCategory();
    }

    @When("^I choose the \"([^\"]*)\" option from the drop-down list$")
    public void iChooseTheOptionFromTheDropDownList(String choosedOption) {
        automationPractice.chooseTheOptionFromTheDropDownList(choosedOption);
    }

    // ------------------------------------------------------------------------------------
    // THEN SECTION
    // ------------------------------------------------------------------------------------

    @Then("^Product listing page should be rendered$")
    public void productListingPageShouldBeRendered() throws Throwable {
        automationPractice.verifyIfProductListingPageIsRendered();
    }

    @Then("^Products should be listed in the right order: \"([^\"]*)\"$")
    public void productsShouldBeListedInTheRightOrder(String order) throws Throwable {
        automationPractice.verifyIfProductsAreListedInTheRightOrder(order);
    }

    @Then("^Products should be properly rendered$")
    public void productsShouldBeProperlyRendered() {
        automationPractice.verifyIfProductsAreProperlyRendered();
    }
}
